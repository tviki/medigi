from django.contrib import admin
from django.urls import path

from . import views
from .views import RegisterPage, CustomLoginView, logout_user
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import LogoutView

app_name = "medic_app"

urlpatterns = [
   path('login/', CustomLoginView.as_view(), name='login'),
   path('logout/', views.logout_user, name='logout'),
   path('register/', RegisterPage.as_view(), name='register'),
   path('', views.index, name='index'),
   
   path('search/', views.search, name='search'),
   path('saved/', views.saved, name='saved'),
   path('profile/<int:pk>/', views.profile_details, name='profile'),
]