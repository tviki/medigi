from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.urls import path, reverse, reverse_lazy
from django.http import HttpResponseRedirect

from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages

from .models import Drug, User, CountryField, Location
from .forms import UserForm, LocationForm, NewUserForm, NewUserForm
from django.db.models import Q



class CustomLoginView(LoginView):
   template_name = 'medic_app/login.html'
   fields = '__all__'
   redirect_authenticated_user = True

   def login_user(request):
      return render(request, 'medic_app/login.html', {})


def logout_user(request):
   logout(request)
   return redirect('medic_app:login')



class RegisterPage(FormView):
   template_name = 'medic_app/register.html'
   form_class = UserCreationForm
   redirect_authenticated_user = True
   success_url = reverse_lazy('medic_app:index')

   def form_valid(self, form):
      user = form.save()
      if user is not None:
         login(self.request, user)
      return super(RegisterPage, self).form_valid(form)


# class DrugList(LoginRequiredMixin, ListView):
#    model = Drug
#    context_object_name = 'saved'
   
#    def get_context_data(self, **kwargs):
#       context = super().get_context_data(**kwargs)
#       context['drugs'] = context['drugs'].filter(user=self.request.user)
#       return context


def index(request):
   if request.user.is_authenticated:
      user = request.user
      drugs = Drug.objects.all
      location_form = LocationForm(request.POST, instance=user.location)
      context = {
         'drugs': drugs,
         'location_form': location_form,
      }
      return render(request, 'medic_app/index.html', context)
   else:
      return HttpResponseRedirect(reverse('medic_app:login'))

# def index(request, pk):
#    if request.user.is_authenticated:
#       return HttpResponseRedirect(reverse('medic_app:index'))
#    else:
#       return HttpResponseRedirect(reverse('medic_app:login'))


# def index(request, pk):
#    user = get_object_or_404(User, pk=pk)
#    if user.is_authenticated:
#       drugs = Drug.objects.all
#       location_form = LocationForm(request.POST, instance=user.location)
#       context = {
#          'drugs': drugs,
#          'location_form': location_form,
#       }
#       return render(request, 'medic_app/index.html', context)



def search(request):
   if request.method == "POST":
      current_user = request.user.location
      countries = CountryField()
      searched = request.POST.get('searched', False)
      try:
         searched_drugs = Drug.objects.get(name__contains=searched)
         drugs = Drug.objects.filter(main_agent_1=searched_drugs.main_agent_1, country=current_user.country)
      except Drug.DoesNotExist:
         searched_drugs = False
         drugs = False
      except Drug.MultipleObjectsReturned:
         searched_drugs = False
         drugs = False
      return render(request, 'medic_app/search_result.html', {'searched': searched, 'drugs': drugs, 'countries': countries,})
   else:
      return render(request, 'medic_app/search_result.html', {})



def profile_details(request, pk):
    user = get_object_or_404(User, pk=pk)
    if request.method == 'GET':
        user_form = UserForm(instance=user)
        location_form = LocationForm(instance=user.location)
    elif request.method == 'POST':
       user_form = UserForm(request.POST, instance=user)
       location_form = LocationForm(request.POST, instance=user.location)
       if user_form.is_valid() and location_form.is_valid():
            user_form.save()
            location_form.save()
    new_user_form = NewUserForm()
    context = {
         'user': user,
         'user_form': user_form,
         'location_form': location_form,
         'new_user_form': new_user_form,
    }
    return render(request, 'medic_app/profile.html', context)

# @login_required
# def updated_user(request, user):
#     if request.method == 'POST':
#         new_user_form = NewUserForm(request.POST)
#         if new_user_form.is_valid():
#             Account.objects.create(user=User.objects.get(pk=user), name=new_account_form.cleaned_data['name'])
#     return HttpResponseRedirect(reverse('bank:staff_customer_details', args=(user,)))


    
def saved(request):
   return render(request, 'medic_app/drug_list.html', {})