from __future__ import annotations
from django.db import models, transaction
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.models.query import QuerySet
from django.conf import settings
from django_countries.fields import CountryField



class Location(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.PROTECT)
    country = CountryField(blank_label='(select country)')

    def __str__(self):
        return f'{self.pk}: {self.country}'


class Drug(models.Model):
    name = models.CharField(max_length=200, db_index=True, blank=True, null=True)
    country= CountryField(blank_label='(select country)')
    main_agent_1 = models.CharField(max_length=200, db_index=True, blank=True, null=True)   
    dose = models.IntegerField(db_index=True, blank=True, null=True)
    unit = models.CharField(max_length=200, db_index=True, blank=True, null=True)
    image = models.ImageField(blank="True", upload_to='media')

    def __str__(self):
        return f'{self.pk}:{self.name}:{self.country}:{self.main_agent_1}:{self.dose}:{self.unit}:{self.image}'


class Favourite(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    drug = models.ForeignKey(Drug, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.pk}:{self.user}:{self.drug}'


