# Generated by Django 4.0.4 on 2022-06-07 17:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medic_app', '0008_rename_searchedcountry_location'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='drug',
            name='main_agent_2',
        ),
        migrations.RemoveField(
            model_name='drug',
            name='main_agent_3',
        ),
        migrations.AddField(
            model_name='drug',
            name='image',
            field=models.ImageField(blank='True', upload_to='media'),
        ),
    ]
