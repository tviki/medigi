# Generated by Django 4.0.4 on 2022-06-07 16:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('medic_app', '0006_alter_drug_dose'),
    ]

    operations = [
        migrations.CreateModel(
            name='SearchedCountry',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('country', django_countries.fields.CountryField(max_length=2)),
            ],
        ),
    ]
