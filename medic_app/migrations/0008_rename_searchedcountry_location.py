# Generated by Django 4.0.4 on 2022-06-07 17:00

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('medic_app', '0007_searchedcountry'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SearchedCountry',
            new_name='Location',
        ),
    ]
