from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from .models import Location


class UserForm(forms.ModelForm):
    username = forms.CharField(label='Username', disabled=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',)

class LocationForm(forms.ModelForm):
    country = forms.CharField(label='Country')

    class Meta:
        model = Location
        fields = ('country',)

class NewUserForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = ('country',)



