from django.contrib import admin
from .models import Drug, Location, Favourite

admin.site.register(Drug)
admin.site.register(Favourite)
admin.site.register(Location)


